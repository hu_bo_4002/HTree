﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTree.Models
{
    public class TreeViewModel
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public int? Level { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int Status { get; set; }

        public List<TreeViewModel> Children { get; set; }

    }
}