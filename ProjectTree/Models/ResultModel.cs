﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTree.Models
{
    public class ResultModel<T>
    {
        public bool IsSuccess { get; set; }

        public string Error { get; set; }

        public List<T> Data { get; set; }
    }
}