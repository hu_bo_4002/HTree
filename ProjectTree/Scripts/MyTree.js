(function ($) {

    var MyTree = function (options) {
        //绑定数据
        this.Data = options.Data;
        //是否开启折叠
        this.ToggleEnable = options.ToggleEnable || false;
        //是否多选
        this.Multiple = options.Multiple || false;
        //默认选中节点
        this.SelectedNode = options.SelectedNode;
        //是否允许拖动
        this.AllowDrag = options.AllowDrag || false;
        //放置事件
        this.onDrop = this.AllowDrag ? options.onDrop : function () { };
        //拖动元素进入目标区域事件
        this.onEnter = this.AllowDrag ? options.onEnter : function () { };
        //拖动元素离开目标区域事件
        this.onLeave = this.AllowDrag ? options.onLeave : function () { };
        //拖动事件
        this.onDrag = this.AllowDrag ? options.onDrag : function () { };
        if (this.ToggleEnable) {
            var self = this;
            $("ul").delegate("a", "click", function () {
                MyTree.prototype.Toggle($(this));
            });
        }
        $("ul").delegate(".tree-node", "click", function () {
            MyTree.prototype.Selected($(this).children(".text-span"), self.Multiple);
        });
        if (this.AllowDrag) {
            var self = this;
            $("ul").delegate(".text-span", "dragstart", function (event) {
                self.onDrag(event);
            });
            $("ul").delegate(".tree-node", "drop", function (event) {
                self.onDrop(event);
            });
            $("ul").delegate(".tree-node", "dragenter", function (event) {
                self.onEnter(event);
            });
            $("ul").delegate(".tree-node", "dragleave", function (event) {
                self.onLeave(event);
            });
            $("ul").delegate(".tree-node", "dragover", function (event) {
                event.originalEvent.preventDefault();
            });
        }
    }
    MyTree.prototype = {
        Init: function (data, par) {
            var json = data || this.Data;
            var self = this;
            for (var i = 0; i < json.length; i++) {
                var attr = json[i];
                var ele = $('<li class="htree-li"></li>');
                var div = $('<div  class="tree-node"></div>');
              
                if (this.SelectedNode && this.SelectedNode == attr.Id) {
                    div.addClass("selected");
                }
                var attrId = $('<div/>').html(attr.Id).text();
                var attrName = $('<div/>').html(attr.Name).text();
                var span;
                if (self.AllowDrag) {
                    span = $('<span draggable="true" data-id="' + attrId + '" class="text-span">' + attrName + '</span>');
                }
                else {
                    span = $('<span data-id="' + attrId + '" class="text-span">' + attrName + '</span>');
                }
                if (attr.Children.length <= 0) {
                    div.append($("<span class='switch-none'></span>"));
                    div.append(span);
                    ele.append(div);
                }
                else {
                    div.append($('<a href="javascript:void(0);" class="switch-open"></a>'));
                    div.append(span);
                    ele.append(div);
                    var nextpar = $("<ul></ul>");
                    ele.append(nextpar);
                    this.Init(attr.Children, nextpar);
                }

                par.append(ele);
            }
        },
        Toggle: function (eve) {

            eve.parent().siblings("ul").toggle();
            if (eve.hasClass("switch-open")) {
                eve.removeClass().addClass("switch-close")
            }
            else {
                eve.removeClass().addClass("switch-open")
            }
        },
        Selected: function (eve, multiple) {
            if (eve.parent(".tree-node").hasClass("selected")) {
                eve.parent(".tree-node").removeClass("selected");
            }
            else {
                if (!multiple) {
                    $(".tree-node").removeClass("selected");
                }
                eve.parent(".tree-node").addClass("selected");
            }

        }
    };
    $.extend($.fn, {
        HTree: function (options) {
            var helper = new MyTree(options);
            helper.Init(null, this);
            var self = this;
            return {
                //获取选中节点的值
                getSelected: function () {
                    var targetDom = $(".selected");
                    var result = new Array();
                    if (options.Multiple) {
                        targetDom.each(function (i, v) {
                            if (v) {
                                result.push($(v).children(".text-span").attr("data-id"));
                            }
                        });
                        return result;
                    }
                    else {
                        if ($(".selected").children(".text-span").attr("data-id")) {
                            result.push($(".selected").children(".text-span").attr("data-id"));
                        }
                        return result;
                    }

                },
                //获取选中节点的文本
                getSelectedValue: function () {
                    var targetDom = $(".selected");
                    var result = new Array();
                    if (options.Multiple) {
                        targetDom.each(function (i, v) {
                            if (v) {
                                result.push($(v).children(".text-span").html());
                            }

                        });
                        return result;
                    }
                    else {
                        if ($(".selected").children(".text-span").html()) {
                            result.push($(".selected").children(".text-span").html());
                        }

                        return result;
                    }
                },
                //选中某个节点
                selected: function (id) {
                    var target = $(".text-span");
                    for (var i = 0; i < target.length; i++) {
                        if ($(target[i]).attr("data-id") == id) {
                            $(target[i]).parent(".tree-node").addClass("selected");
                            break;
                        }
                    }

                },
                //重新加载
                reload: function (data) {
                    $(self).empty();
                    helper.Init(data, self);
                }
            };
        }
    });
})(jQuery);

