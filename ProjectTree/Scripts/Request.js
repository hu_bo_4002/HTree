﻿(function (window) {
    window.core = {};
    window.modules = $.extend((window.modules || {}), {
        ajax: {
            beforeSend: function () {
            },
            afterComplete: function (data) {
                if (data.Redirect) {
                    location = data.RedirectUrl;
                }
            }
        }
    });
    core.ajax = function (url, data, requestMethod, contentType, cb) {
        window.jQuery && $.ajax({
            url: url,
            beforeSend: function () {
               
                window.modules.ajax.beforeSend();
            },
            contentType: contentType,
            dataType: 'json',
            data: data,
            type: requestMethod,
            complete: function (XMLHttpRequest, textStatus) {
                var data;
                try {
                    if (XMLHttpRequest.responseText != "") {
                        data = JSON.parse(XMLHttpRequest.responseText);
                    }
                    else {
                        data = XMLHttpRequest.responseText;
                    }
                }
                catch (err) {
                    data = { isSuccess: false, Msg: "对不起，处理您的请求时出现服务器错误，请稍后再试" }
                }
                window.modules.ajax.afterComplete(data);
                cb(data);
         
            }
        });
    }
  
    
    core.get = function (url, data, cb) {
        this.ajax(url, data, 'get', 'application/json', cb);
    };
    core.post = function (url, data, cb) {
        data = JSON.stringify(data);
        this.ajax(url, data, 'post', 'application/json', cb);
    };   
})(window);
