﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBModel;
using ProjectTree.Models;

namespace ProjectTree.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(Guid id)
        {
            using (var context = new TreeEntities())
            {
                var data = context.TreeInfoes.FirstOrDefault(i => i.Id == id);
                return View(data);
            }
        }



        #region 加载树
        public JsonResult GetTreeData()
        {
            using (var context = new TreeEntities())
            {
                var list = context.TreeInfoes.Where(i => i.Status == 1).OrderBy(i => i.Level).ToList();
                var result = LoadTree(null, list);
                return Json(new { list = result });
            }
        }

        private List<TreeViewModel> LoadTree(Guid? parentId, List<TreeInfo> list)
        {
            List<TreeViewModel> result = new List<TreeViewModel>();
            foreach (var item in list)
            {
                if (item.ParentId != parentId)
                    continue;
                TreeViewModel model = new TreeViewModel();
                model.Id = item.Id;
                model.ParentId = item.ParentId;
                model.Name = item.Name;
                model.Code = item.Code;
                model.Level = item.Level;
                model.CreatedDate = item.CreatedDate;
                model.UpdateDate = item.UpdateDate;
                model.Children = LoadTree(item.Id, list).OrderBy(i => i.Level).ToList();
                result.Add(model);
            }
            return result;
        }
        #endregion

        #region 新增/编辑/删除节点

        public JsonResult CreateNode(string name, Guid? parentId, int order)
        {
            ResultModel<object> result = new ResultModel<object>() { IsSuccess = false };
            if (string.IsNullOrEmpty(name))
            {
                result.Error = "节点名称不能为空";
                return Json(result);
            }

            using (var context = new TreeEntities())
            {
                TreeInfo info = new TreeInfo()
                {
                    Id = Guid.NewGuid(),
                    ParentId = parentId,
                    Name = name,
                    Code = BuildCode(context, parentId),
                    CreatedDate = DateTime.Now,
                    Level = order,
                    Status = 1
                };
                var children = context.TreeInfoes.Where(i => i.ParentId == parentId).ToList();
                if (children != null && children.Any())
                {
                    children.ForEach(i =>
                    {
                        if (i.Level >= order)
                        {
                            i.Level = i.Level + 1;
                            context.Set<TreeInfo>().Attach(i);
                            context.Entry(i).State = System.Data.Entity.EntityState.Modified;
                        }

                    });
                }
                context.TreeInfoes.Add(info);
                result.IsSuccess = context.SaveChanges() > 0;
            }
            return Json(result);
        }

        private string BuildCode(TreeEntities context, Guid? parentId)
        {
            //找到父级和兄弟节点集合
            var mappingList = context.TreeInfoes.Where(i => i.Id == parentId || i.ParentId == parentId).ToList();
            var maxCode = mappingList != null && mappingList.Any() ? mappingList.Max(i => i.Code) : "001";
            //有兄弟节点有父节点或者有兄弟节点没有父节点，找兄弟节点的最大值
            if (mappingList.Count > 1 || (mappingList.Count == 1 && !parentId.HasValue))
            {
                var newCode = (Convert.ToUInt64(maxCode) + 1).ToString();
                if (newCode.Length > 3)
                {
                    newCode = "00" + newCode;
                }
                else if (newCode.Length == 2)
                {
                    newCode = "0" + newCode;
                }
                else if (newCode.Length == 1)
                {
                    newCode = "00" + newCode;
                }
                return newCode;
            }
            //有父节点没有兄弟节点
            else if (mappingList.Count == 1 && parentId.HasValue)
            {
                var newCode = maxCode + "001";
                return newCode;
            }

            else
            {
                return maxCode;
            }

        }
        private List<TreeInfo> GetAllChildren(TreeEntities context, Guid id)
        {
            var sql = @"
with cte as
(
select 
	*
from 
	dbo.TreeInfo
where 
	ParentId =@ParentId
union all
select 
	b.*
from 
	cte a
inner join 
	dbo.TreeInfo b
    on a.Id=b.ParentId
)
select *
from cte 
";
            var par = new List<SqlParameter>();
            par.Add(new SqlParameter("@ParentId", id));
            var list = context.Database.SqlQuery<TreeInfo>(sql, par.ToArray()).ToList();
            return list;

        }
        public JsonResult EditNode(Guid id, string name, Guid? parentId, int moveWay, int order)
        {
            ResultModel<object> result = new ResultModel<object>() { IsSuccess = false };
            using (var context = new TreeEntities())
            {
                var dbData = context.TreeInfoes.FirstOrDefault(i => i.Id == id);
                if (dbData == null)
                {
                    result.Error = "未能找到对应的数据";
                    return Json(result);
                }
                if (parentId != dbData.ParentId)
                {
                    if (moveWay == 0)
                    {
                        var children = context.TreeInfoes.Where(i => i.ParentId == dbData.Id).ToList();
                        if (children != null && children.Any())
                        {
                            children.ForEach(i =>
                            {
                                i.ParentId = dbData.ParentId;
                                context.Set<TreeInfo>().Attach(i);
                                context.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            });
                        }
                    }
                    else
                    {
                        //获取最所有子集，验证在整体移动下是否选择了自己原有的子级作为父级
                        var list = GetAllChildren(context, id);
                        var children = list != null && list.Any() ? list.Select(i => i.Id).ToList() : null;
                        if (children != null)
                        {
                            if (children.Contains(parentId.GetValueOrDefault()))
                            {
                                result.Error = "在整体移动的情况下不能选择自己的子级作为父级";
                                return Json(result);
                            }
                        }
                    }
                    var childList = context.TreeInfoes.Where(i => i.ParentId == parentId).ToList();
                    if (childList != null && childList.Any())
                    {
                        childList.ForEach(i =>
                        {
                            if (i.Level >= order)
                            {
                                i.Level = i.Level + 1;
                                context.Set<TreeInfo>().Attach(i);
                                context.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            }

                        });
                    }

                }

                dbData.Name = name;
                dbData.UpdateDate = DateTime.Now;
                dbData.ParentId = parentId;
                dbData.Code = BuildCode(context, parentId);
                dbData.Level = order;
                context.Set<TreeInfo>().Attach(dbData);
                context.Entry(dbData).State = System.Data.Entity.EntityState.Modified;
                result.IsSuccess = context.SaveChanges() > 0;
            }
            return Json(result);
        }

        public JsonResult DeleteNodes(Guid id)
        {
            ResultModel<object> result = new ResultModel<object>() { IsSuccess = false };
            using (var context = new TreeEntities())
            {
                var data = context.TreeInfoes.FirstOrDefault(i => i.Id == id && i.Status == 1);
                if (data == null)
                {
                    result.Error = "未能找到对应的数据";
                    return Json(result);
                }
                var list = GetAllChildren(context, id);
                data.Status = 0;
                data.UpdateDate = DateTime.Now;
                context.Set<TreeInfo>().Attach(data);
                context.Entry(data).State = System.Data.Entity.EntityState.Modified;
                list.ForEach(item =>
                {
                    item.Status = 0;
                    context.Set<TreeInfo>().Attach(item);
                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                });
                result.IsSuccess = context.SaveChanges() > 0;


            }
            return Json(result);
        }
        public JsonResult ChangePosition(Guid sourceId, Guid targetId)
        {
            ResultModel<TreeViewModel> result = new ResultModel<TreeViewModel>() { IsSuccess = false };
            using (var context = new TreeEntities())
            {
                var list = context.TreeInfoes.Where(i => i.Id == sourceId || i.Id == targetId).ToList();
                var sourceData = list.FirstOrDefault(i => i.Id == sourceId);
                if (sourceData == null)
                {
                    result.Error = "未能找到原始节点数据";
                    return Json(result);
                }
                var targetData = list.FirstOrDefault(i => i.Id == targetId);
                if (targetData == null)
                {
                    result.Error = "未能找到目标节点数据";
                    return Json(result);
                }

                if (sourceData.ParentId != targetData.ParentId)
                {
                    var children = context.TreeInfoes.Where(i => i.ParentId == sourceId || i.ParentId == targetId).ToList();
                    if (children != null && children.Any())
                    {
                        var sourceChildren = children.Where(i => i.ParentId == sourceId && i.Id != targetId).ToList();
                        var targetChildren = children.Where(i => i.ParentId == targetId && i.Id != sourceId).ToList();
                        if (sourceChildren != null && sourceChildren.Any())
                        {
                            sourceChildren.ForEach(i =>
                            {
                                i.ParentId = targetId;
                                context.Set<TreeInfo>().Attach(i);
                                context.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            });
                        }
                        if (targetChildren != null && targetChildren.Any())
                        {
                            targetChildren.ForEach(i =>
                            {
                                i.ParentId = sourceId;
                                context.Set<TreeInfo>().Attach(i);
                                context.Entry(i).State = System.Data.Entity.EntityState.Modified;
                            });
                        }


                    }

                    //子级和父级交换位置，子级节点为源节点，目标节点的parentId应该变为sourceId
                    if (sourceData.ParentId == targetId)
                    {
                        sourceData.ParentId = targetData.ParentId;
                        targetData.ParentId = sourceId;

                    }
                    //子级和父级交换位置，子节点为目标节点，源节点的ParentId应该变为targetId
                    else if (targetData.ParentId == sourceId)
                    {
                        targetData.ParentId = sourceData.ParentId;
                        sourceData.ParentId = targetId;
                    }
                    else
                    {
                        var sourceParentId = sourceData.ParentId;
                        sourceData.ParentId = targetData.ParentId;
                        targetData.ParentId = sourceParentId;
                    }

                }
                var sourceLevel = sourceData.Level;
                sourceData.Level = targetData.Level;
                targetData.Level = sourceLevel;
                context.Set<TreeInfo>().Attach(sourceData);
                context.Entry(sourceData).State = System.Data.Entity.EntityState.Modified;
                context.Set<TreeInfo>().Attach(targetData);
                context.Entry(targetData).State = System.Data.Entity.EntityState.Modified;

                result.IsSuccess = context.SaveChanges() > 0;
                var newList = context.TreeInfoes.Where(i => i.Status == 1).OrderBy(i => i.Level).ToList();
                result.Data = LoadTree(null, newList);

            }
            return Json(result);
        }
        public JsonResult UpMove(Guid id)
        {
            ResultModel<TreeViewModel> result = new ResultModel<TreeViewModel>() { IsSuccess = false };
            using (var context = new TreeEntities())
            {
                var data = context.TreeInfoes.FirstOrDefault(i => i.Id == id);
                if (data == null)
                {
                    result.Error = "未能找到对应的数据";
                    return Json(data);
                }
                var brotherList = context.TreeInfoes.Where(i => i.ParentId == data.ParentId && i.Level < data.Level).OrderByDescending(i => i.Level).ToList();
                if (brotherList == null || !brotherList.Any())
                {
                    result.Error = "当前节点排位已经是最靠前，不能再继续上移";
                    return Json(result);
                }
                var secondBrother = brotherList.FirstOrDefault();
                var curLevel = data.Level;

                data.Level = secondBrother.Level;
                secondBrother.Level = curLevel;
                context.Set<TreeInfo>().Attach(data);
                context.Entry(data).State = System.Data.Entity.EntityState.Modified;
                context.Set<TreeInfo>().Attach(secondBrother);
                context.Entry(secondBrother).State = System.Data.Entity.EntityState.Modified;
                result.IsSuccess = context.SaveChanges() > 0;

                var newList = context.TreeInfoes.Where(i => i.Status == 1).OrderBy(i => i.Level).ToList();
                result.Data = LoadTree(null, newList);

            }
            return Json(result);
        }
        public JsonResult DownMove(Guid id)
        {
            ResultModel<TreeViewModel> result = new ResultModel<TreeViewModel>() { IsSuccess = false };
            using (var context = new TreeEntities())
            {
                var data = context.TreeInfoes.FirstOrDefault(i => i.Id == id);
                if (data == null)
                {
                    result.Error = "未能找到对应的数据";
                    return Json(data);
                }
                var brotherList = context.TreeInfoes.Where(i => i.ParentId == data.ParentId && i.Level > data.Level).OrderBy(i => i.Level).ToList();
                if (brotherList == null || !brotherList.Any())
                {
                    result.Error = "当前节点排位已经是最靠后，不能再继续下移";
                    return Json(result);
                }
                var secondBrother = brotherList.FirstOrDefault();
                var curLevel = data.Level;

                data.Level = secondBrother.Level;
                secondBrother.Level = curLevel;
                context.Set<TreeInfo>().Attach(data);
                context.Entry(data).State = System.Data.Entity.EntityState.Modified;
                context.Set<TreeInfo>().Attach(secondBrother);
                context.Entry(secondBrother).State = System.Data.Entity.EntityState.Modified;
                result.IsSuccess = context.SaveChanges() > 0;

                var newList = context.TreeInfoes.Where(i => i.Status == 1).OrderBy(i => i.Level).ToList();
                result.Data = LoadTree(null, newList);
            }
            return Json(result);
        }


        #endregion
    }
}