USE [BrcPaaS]
GO
/****** Object:  Table [dbo].[TreeInfo]    Script Date: 2020/4/23 11:11:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TreeInfo](
	[Id] [uniqueidentifier] NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Code] [nvarchar](200) NOT NULL,
	[Level] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]

GO
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'39ebf198-e83e-4614-9e49-5a4ce30e2bb1', NULL, N'家电', N'003', 1, CAST(0x0000ABA6009EDC65 AS DateTime), CAST(0x0000ABA600A694B8 AS DateTime), 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'2aac1ab7-c59e-4ffa-8a73-e14cb83447b7', N'39ebf198-e83e-4614-9e49-5a4ce30e2bb1', N'厨卫', N'003001', 10003, CAST(0x0000ABA6009F0006 AS DateTime), CAST(0x0000ABA600A6A564 AS DateTime), 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'246e884e-daf7-47cd-aafa-144a7d7ce3b5', N'39ebf198-e83e-4614-9e49-5a4ce30e2bb1', N'办公', N'003002', 10004, CAST(0x0000ABA6009F16E5 AS DateTime), CAST(0x0000ABA600A6B402 AS DateTime), 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'7e3c274b-6863-4699-a4a0-6afeb124ddab', N'39ebf198-e83e-4614-9e49-5a4ce30e2bb1', N'游戏', N'003003', 10005, CAST(0x0000ABA6009F2697 AS DateTime), CAST(0x0000ABA600A6BCB3 AS DateTime), 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'5dc701c7-7a4d-47d2-a360-424e5f95653b', NULL, N'书籍', N'002', 3, CAST(0x0000ABA6009F5A8A AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'8ecf2829-743f-4d2d-abb2-13905403b59f', N'5dc701c7-7a4d-47d2-a360-424e5f95653b', N'科幻', N'002001', 20001, CAST(0x0000ABA6009F6954 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'dfd28eb4-d003-49c5-bf76-0f1a00243212', N'5dc701c7-7a4d-47d2-a360-424e5f95653b', N'灵异', N'002002', 20002, CAST(0x0000ABA6009F7AAC AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'c8f50501-41d1-43cc-a613-de7b89817563', N'5dc701c7-7a4d-47d2-a360-424e5f95653b', N'都市', N'002003', 20004, CAST(0x0000ABA6009F8B22 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'8ff25f57-71b1-4d5a-9e04-2ff9e240fefc', N'5dc701c7-7a4d-47d2-a360-424e5f95653b', N'武侠', N'002004', 20003, CAST(0x0000ABA6009F9BD7 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'7a748686-b396-4e06-bdb1-89878598eab2', N'2aac1ab7-c59e-4ffa-8a73-e14cb83447b7', N'抽油烟机', N'001001001', 100010001, CAST(0x0000ABA6009FB5A1 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'0ca56f42-3a06-445e-a402-6b5f2b5e0a57', N'2aac1ab7-c59e-4ffa-8a73-e14cb83447b7', N'燃气灶', N'001001003', 100010002, CAST(0x0000ABA6009FCE1E AS DateTime), CAST(0x0000ABA6009FE787 AS DateTime), 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'044718cf-d419-4f48-8bd2-19def51d435a', N'2aac1ab7-c59e-4ffa-8a73-e14cb83447b7', N'电水壶', N'001001004', 100010003, CAST(0x0000ABA600A00BBB AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'c4f5dfeb-29d4-4bae-9e0c-159cd6a4af1c', N'2aac1ab7-c59e-4ffa-8a73-e14cb83447b7', N'热水器', N'001001005', 100010004, CAST(0x0000ABA600A019CD AS DateTime), CAST(0x0000ABA600A1A644 AS DateTime), 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'04f02548-b6dc-45b5-920a-bd1baf3aefd5', N'246e884e-daf7-47cd-aafa-144a7d7ce3b5', N'电脑', N'001002001', 100020001, CAST(0x0000ABA600A02F15 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'0ac1af20-e533-4008-8dcb-f9c4b8f05187', N'246e884e-daf7-47cd-aafa-144a7d7ce3b5', N'U盘', N'001002002', 100020002, CAST(0x0000ABA600A043C8 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'a52619a4-5ff2-444d-99f6-d84e2d6ef2db', N'246e884e-daf7-47cd-aafa-144a7d7ce3b5', N'手机', N'001002003', 100020003, CAST(0x0000ABA600A05AF8 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'00f3bfbd-b39c-4901-ae7d-b414f84ba4d6', N'7e3c274b-6863-4699-a4a0-6afeb124ddab', N'PS4', N'001003001', 100030001, CAST(0x0000ABA600A066D1 AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'24a51fe2-9ba6-48d9-ab41-8d22a9c3b83f', N'7e3c274b-6863-4699-a4a0-6afeb124ddab', N'Xbox', N'001003002', 100030002, CAST(0x0000ABA600A0826A AS DateTime), NULL, 1)
INSERT [dbo].[TreeInfo] ([Id], [ParentId], [Name], [Code], [Level], [CreatedDate], [UpdateDate], [Status]) VALUES (N'9d65f7e8-afe0-482e-9e32-903c2e0ae709', N'7e3c274b-6863-4699-a4a0-6afeb124ddab', N'GameBoy', N'001003004', 100030003, CAST(0x0000ABA600A09DF1 AS DateTime), NULL, 1)
