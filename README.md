# HTree

#### 介绍
核心为基于jquery实现的一个“可拖动树形插件”
插件路径为："/Scripts/MyTree.js"
示例页面路径为："/Views/Home/Index.cshtml"
其余代码均为demo演示



#### 软件架构
DEMO编写使用.NET4.6
ORM:EF6
数据库:SqlServer

#### 安装教程

1.  创建一个数据库，使用新创建的数据库执行HTree.sql创建表及添加数据
2.  修改web.config中的数据库配置信息
data source：数据库地址("."表示本地)
initial catalog：数据库名称（示例中为"demo"）
user id：数据库登录名（示例中为"sa"）
password：数据库密码（示例中为"123456Aa"）
<connectionStrings>
    <add name="TreeEntities" connectionString="metadata=res://*/TreeModel.csdl|res://*/TreeModel.ssdl|res://*/TreeModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=.;initial catalog=demo;persist security info=True;user id=sa;password=123456Aa;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
</connectionStrings>

#### 使用说明

1.  拖动时请将鼠标放在"节点文字"上再拖动
2.  若发现bug请及时给与反馈


